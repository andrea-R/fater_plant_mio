import os

this_dir = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(this_dir, "data")
EVAL_DIR = os.path.join(DATA_DIR, "eval")

# Folder data paths
    #raw
DATA_RAW_PATH = os.path.join(DATA_DIR, "raw")
DATA_CLEAN_PATH = os.path.join(DATA_DIR, "clean")

ORC_RAW_PATH = os.path.join(DATA_RAW_PATH, 'ORC')
SCR_RAW_PATH = os.path.join(DATA_RAW_PATH, 'SCR')
SME_RAW_PATH = os.path.join(DATA_RAW_PATH, 'SME')
PRODUCTION_PLANS_RAW_PATH = os.path.join(DATA_RAW_PATH, "Piani_di_produzione")
DOWNTIME_CAUSES = os.path.join(PRODUCTION_PLANS_RAW_PATH, "production_plan.csv")
TEMPERATURE_RAW_PATH = os.path.join(DATA_RAW_PATH, 'Temperature')

ORC_JOINED_PATH = os.path.join(DATA_RAW_PATH, 'orc_joined.p.gzip')

    #clean
DOWNTIME_CAUSES_CLEAN = os.path.join(DATA_CLEAN_PATH, "downtime_causes.p")
TEMPERATURE_CLEAN_PATH = os.path.join(DATA_CLEAN_PATH, 'Temperature')
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 15:50:52 2020

Read netcdf file for temperature and export to .csv

@author: neri.a
"""
import netCDF4 as nc
import pandas as pd
import numpy as np
import datetime as dt
import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))
from config import TEMPERATURE_RAW_PATH, TEMPERATURE_CLEAN_PATH


file_name = "tg_ens_mean_0.1deg_reg_2011-2019_v20.0e.nc"
file_path = os.path.join(TEMPERATURE_RAW_PATH,file_name)
print("Loading file from "+file_path)
layer_name = "tg"

data = nc.Dataset(file_path,'r',format="NETCDF4")
lon = data.variables['longitude'][:]
lat = data.variables['latitude'][:]
time = data.variables['time'] # expressed as number of days since origin
# extract origin of days count
start_time = pd.to_datetime(time.units[11:9999])
time=time[:]
# sum the days to the origin 
dates = [start_time + pd.Timedelta(x,"D") for x in time]
pd.Timedelta
# These are the WGS84 coordinates of the Fater production plant in Pescara
myCoord = {'x':14.184259,
           'y':42.442867}

# take the index of long and lat of the .nc file
x_loc = np.argmax(myCoord['x']<lon)
y_loc = np.argmax(myCoord['y']<lat)

table = pd.DataFrame({'Date':dates,
                      'Temperature':data["tg"][:,y_loc,x_loc]})

# select only dates higher then filter_date
filter_date = pd.to_datetime("2015-01-01")
table = table.loc[table.Date>=filter_date]
start_date = str(min(table.Date))[0:10]
end_date = str(max(table.Date))[0:10]

print ("Writing file in "+TEMPERATURE_CLEAN_PATH)
table.to_csv(os.path.join(TEMPERATURE_CLEAN_PATH,("Temp_"+start_date+"_to_"+end_date+".csv")), index=False)
